type btnType = {
  varient: "pri" | "sec";
} & React.ComponentProps<"button">;

export function CustomBtn({ varient, children, ...rest }: btnType) {
  return (
    <button className={varient} {...rest}>
      {children}
    </button>
  );
}
