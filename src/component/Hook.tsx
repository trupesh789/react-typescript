import { useState } from "react";

export function Hook() {
  const [user, setUser] = useState<{ name: string; id: number } | null>(null);

  return (
    <div>
      {user ? <h2>Hello {user.name}</h2> : <h2>Hello guest</h2>}
      <button onClick={() => setUser({ name: "trupesh", id: 1 })}>Login</button>
      <button onClick={() => setUser(null)}>Logout</button>
    </div>
  );
}
