import React from "react";

type containarType = {
  children: React.ReactNode;
  styleProp: React.CSSProperties;
};

export function Containar(props: containarType) {
  return <div style={props.styleProp}>{props.children}</div>;
}
