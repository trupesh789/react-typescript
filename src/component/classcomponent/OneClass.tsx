import { Component, ReactNode } from "react";

type classProps = {
  messageProp: string;
};

type classState = {
  count: number;
};

export class OneClass extends Component<classProps, classState> {
  state = {
    count: 0,
  };

  funclass = () => {
    this.setState((prevState) => ({ count: prevState.count + 1 }));
  };

  render(): ReactNode {
    // console.log(this);

    return (
      <div>
        <h2>
          {this.props.messageProp} {this.state.count}
        </h2>
        <button onClick={this.funclass}>ClassCompo +1</button>
      </div>
    );
  }
}
