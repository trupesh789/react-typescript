// type propType = {
//   name: string;
//   eId: number;
// };

type propType = {
  nameArray: {
    name: string;
    eId: number;
  }[];
};

export function Compo(props: propType) {
  return (
    <div>
      {props.nameArray.map((d) => (
        <h1>Heelo {d.name}</h1>
      ))}
    </div>
  );
}
