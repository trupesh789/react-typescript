import { createContext } from "react";

export const themeValue = {
  main: {
    p: "red",
    s: "green",
  },
};

export const ThemContext: any = createContext(themeValue);

export function ThemContextProvider({ children }: typeProvider) {
  return (
    <ThemContext.Provider value={themeValue}>{children}</ThemContext.Provider>
  );
}

type typeProvider = {
  children: React.ReactNode;
};
