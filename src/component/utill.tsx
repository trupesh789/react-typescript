type apiCallpara = {
  url: string;
  isTrue: number;
};

export function apiCall({ url, isTrue }: apiCallpara) {
  console.log(url, isTrue);
}

export function getFullname(fName: string, lName?: string) {
  if (lName) {
    return `My First name is ${fName} My Last Name is ${lName}`;
  }

  return `My First name is ${fName}`;
}
