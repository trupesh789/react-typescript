function Type() {
  // let name: any;
  // name = "hello";
  // console.log(name);

  // let unianType: string | boolean;

  // unianType = "hello";
  // unianType = true;

  // let heelo: unknown = 20;
  // let heelo: any = 20;

  // apiCall({
  //   url: "api.asd.com/login",
  //   isTrue: 21,
  // });

  // const fullname: string = getFullname("Trupesh", "lastName");

  // let arrayNumber: Array<number> = [12, 3];

  // const arrString: Array<string> = ["heelo", "lol"];

  // const arrString: Array<unknown> = ["heelo", 20];

  // const objj: {} = {
  //   has: "asdasd",
  // };

  // console.log(objj);

  // const arr: { name: string; eId: number }[] = [
  //   {
  //     name: "trupesh",
  //     eId: 10,
  //   },
  //   {
  //     name: "test",
  //     eId: 20,
  //   },
  // ];

  // const context: any = useContext(ThemContext);

  // console.log(context.main);

  return (
    <div className="Type">
      <h1>Types</h1>
    </div>
  );
}

export default Type;
