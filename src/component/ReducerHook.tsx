import { useReducer } from "react";

type stateType = { count: number; name?: string };

type updateActionType = { type: "INC" | "RESET" | "DEC"; payload: number };

type ResetActionType = { type: "RESET" };

type actionType = updateActionType | ResetActionType;

export default function ReducerHook() {
  const initialState = { count: 0, name: "hello" };

  function reducer(state: stateType = initialState, action: actionType) {
    switch (action.type) {
      case "INC":
        return { ...state, count: state.count + action.payload, name: "lol" };
      case "DEC":
        return { ...state, count: state.count - action.payload };
      case "RESET":
        return initialState;

      default:
        return state;
    }
  }

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div>
      <h1>Hello reducer: {state?.name}</h1>
      <h1>Count: {state?.count}</h1>

      <button onClick={() => dispatch({ type: "INC", payload: 5 })}>+</button>
      <button onClick={() => dispatch({ type: "RESET" })}>Reset</button>
      <button onClick={() => dispatch({ type: "DEC", payload: 5 })}>-</button>
    </div>
  );
}

// function inc(data: any) {
//   return {
//     type: "INC",
//     payload: data.payload,
//   };
// }
