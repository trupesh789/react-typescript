import React, { useRef } from "react";

export function UseRefHook() {
  const inputRef = useRef<HTMLInputElement>(null!);

  // useEffect(() => {
  //   inputRef.current.focus();
  // }, []);

  return (
    <div>
      <input type="text" ref={inputRef} />
    </div>
  );
}
