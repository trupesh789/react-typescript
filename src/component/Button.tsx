import React from "react";

type buttonProps = {
  handleClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  children: string;
};

export function Button(props: buttonProps) {
  return <button onClick={props.handleClick}>{props.children}</button>;
}
