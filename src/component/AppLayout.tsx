// if child is String
// type applayoutType = {
//   children: string;
// };

// if child is Compo

type applayoutType = {
  children: React.ReactNode;
};

export function AppLayout(props: applayoutType) {
  const { children } = props;
  return (
    <>
      <h2>Nav Bar</h2>
      <h2>Side bar</h2>
      {children}
    </>
  );
}
