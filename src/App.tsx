import { Compo } from "./component/Compo";
import { AppLayout } from "./component/AppLayout";
import Type from "./component/Type";
import { SidBar } from "./component/SidBar";
import { Button } from "./component/Button";
import Input from "./component/Input";
import { Containar } from "./component/Containar";
import { Hook } from "./component/Hook";
import ReducerHook from "./component/ReducerHook";
import { ThemContextProvider } from "./component/context/ThemeContext";
import { UseRefHook } from "./component/UseRefHook";
import { OneClass } from "./component/classcomponent/OneClass";
import { CustomBtn } from "./component/CustomBtn";

function App() {
  // const nameObj = {
  //   fName: "Trupesh",
  //   eId: 10,
  // };

  const arr = [
    {
      name: "trupesh",
      eId: 10,
    },
    {
      name: "test",
      eId: 20,
    },
  ];

  return (
    <ThemContextProvider>
      <Containar styleProp={{ margin: "20px" }}>
        <OneClass messageProp="Hello class count:" /> <br />
        <CustomBtn varient="pri" onClick={() => console.log("helllo btn")}>
          Pri btn
        </CustomBtn>
        <Hook />
        <Type />
        <Compo nameArray={arr} />
        <AppLayout>Home</AppLayout>
        <AppLayout>
          <SidBar />
        </AppLayout>
        <Button handleClick={(e) => console.log("test btn", e)}>
          Side bar btn
        </Button>
        <Input
          value=""
          handleChange={(e) => console.log("test input", e.target.value)}
        />
        <br />
        <ReducerHook />
        <UseRefHook />
      </Containar>
    </ThemContextProvider>
  );
}

export default App;
